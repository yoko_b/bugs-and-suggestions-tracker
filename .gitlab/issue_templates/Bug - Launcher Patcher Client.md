<!-- This template is meant for patcher issues like failing to download updates, failing to write it and so on. -->

## Summary

<!-- Add a brief description of the bug (between two to three lines) under this line -->

## Steps to reproduce

1.
2.
3. (Add more points as needed)

## Actual results

<!-- Add a description of what happens after the steps described above under this line -->

## Expected results

<!-- Add a description of what should happen instead under this line -->

## Additional information

<!-- Add any additional information if applicable under this line -->

### Computer details

- Operating System and Version:
- Laptop or Desktop computer:
- Country of connection:
- Installtion Path (Optional):
- CPU (Optional):
- GPU (Optional):
- Ram (Optional):

### Notes

<!-- Any extra notes? feel free to share it under this line -->
