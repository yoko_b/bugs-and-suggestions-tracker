**Disclaimer** During beta period some specific systems might be not working as intended, not working at all or abusable, for any abusable bugs found we will ensure all of it effect is reverted back and if necessary rollback characters.

**This maintenance was focused on bug fixes, as we received tons of reports some being seriously game breaking we decided to focus on this first before releasing new content**

# Content
* Eden quests for the range 85 to 95 has been added
* Changed requirements for 2nd job change to 50 Base level and job level
* First job change quests now grants platinum skills, please note this is not applied to whom change through izlude academy
* Status and skill reset is now available, every time you change your job you're granted 1 free skill and status reset (they don't stack), a grace period of 90 days after each reset is applied till next reset, changing your job however bypasses this grace period
* Memo has been allowed in the following maps
	* cmd_fild01
	* cmd_fild02
	* cmd_fild03
	* cmd_fild04
	* cmd_fild05
	* cmd_fild06
	* cmd_fild07
	* cmd_fild08
	* cmd_fild09
	* gef_fild02
	* gef_fild06
	* gef_fild08
	* gef_fild12
	* gef_fild14
	* mjolnir_01
	* mjolnir_02
	* mjolnir_03
	* mjolnir_05
	* mjolnir_07
	* mjolnir_08
	* mjolnir_11

# Fixes
* Fixed various bugs in the izlude academy
* Fixed memory violations in quest system which caused the server to crash for few times on launch
* Fixed Eden quests not checking for minimum level in turn-in missions
* Fixed Eden quests timer not being checked after first time it expires which caused players to be able to turn in items instantly
* Fixed a memory violation in party exp share checks caused server to crash in rare cases
* Fixed newly created parties fails to check for unique master accounts until a member leaves the party
* Fixed Imposito Manus affecting raw damage output instead of weapon attack
* Fixed Alchemist missing NPCs which are supposed to be in juno, they're moved now to Aldebaran.
* Removed Super Novice job change, players whom have super novice in their account have their account locked, they will need to contact a Game Master to change their job to a different desired job
* Fixed increase agility not adding the 1% ASPD bonus per level as expected
* Fixed Two Hand Quicken, One Hand Quicken, Adrenaline Rush, Berserk, Madness Canceller, Spear quicken, Mercenary Quicken and Homunculus skill Fleet ASPD bonus
* Fixed Musical Instruments and Whips dropped option group
* Fixed Blacksmith Greed platinum skill quest not correctly checking for left weight and requiring 499 instead of 500 weight
* Fixed Niflhiem missing warps
* Removal of binoculars headgear quest as it was not intended to be released
* Fixed sage platinum skill quest, the NPC can now be found in Geffen tower somewhere!
* Disabled monster experience in the Poring land of the new novice academy
* Fixed Awakening Potion, Berserk Potion and similar item ASPD bonus, it should now increase your ASPD correctly.
* Fixed stolen items missing options, all newly stolen items shall now be granted options.
