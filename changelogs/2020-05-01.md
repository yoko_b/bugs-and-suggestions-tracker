**Disclaimer** During beta period some specific systems might be not working as intended, not working at all or abusable, for any abusable bugs found we will ensure all of it effect is reverted back and if necessary rollback characters.

**This update was focused on bringing fixes to recently released content**

# Content
* Most common weapon drops has been updated to drop at 5%
* Spawn rates has been further adjusted, we're looking for further feedback.
* Double Attack can crit now

# Fixes
* Autoloot would no longer loot items out of view area or on death.
* Brandish Spear is now hitting 3 times as expected and it damage is counted as Ranged attack.
* Magnus Exorcismus now have 6 seconds cooldown as expected.
* Bowling Bash, Brandish Spear and Sonicblow cooldown shall work as expected.
* Earth Spike is corrected, and lowered to 200% MATK (before patch 300%).
* Sandman shall no longer bully you, Earth Spike shall now hit you 3 times at similar attack to Zero.
* Blacksmiths Ranked 5 or higher will recieve the top tier options now.
* Champion slaves would no longer drop experience or loot.
* Owl Duke would not lose it boss status randomly anymore.
* Storm Gust shall now freeze targets at 150% chance on third hit (100% previously)
* Impositio Manus and Suffragium cooldown lowered to 30 seconds
