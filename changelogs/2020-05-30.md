**Disclaimer** During beta period some specific systems might be not working as intended, not working at all or abusable, for any abusable bugs found we will ensure all of it effect is reverted back and if necessary rollback characters.

**This update was focused on bringing fixes to recently released content, QoL updates and slight content changes**

# Quality of Life
## Character storage
Account storage has been replaced with character storage, we have moved the current account storage to the highest level/highest class character in your account this update will allow players to collect as much items as they wish to and especially useful for guilds, each character storage will offer 600 slots just like normal account storage.

## Master storage
Master storage has been implemented unlike Account storage which is suppressed by character storage now it's shared among all your master account game accounts allowing smooth movements of items between game accounts.

## Guild storage
Guild storage is a stable future in private servers we introduced this and now it can be accessed from all kafras, guild leaders can assign access permission through the positions list.

## Monster Database
We have introduced a fully featured monster database available now at our control panel and can be accessed through [https://cp.playragnarokzero.com/monster/](https://cp.playragnarokzero.com/monster/)

## Item database
We have introduced a fully featured item database available now at our control panel and can be accessed through [https://cp.playragnarokzero.com/item/](https://cp.playragnarokzero.com/item/)

## Vendors and Buying stores
We have introduced a fully features market database available now at our control panel and can be accessed through [https://cp.playragnarokzero.com/market/](https://cp.playragnarokzero.com/market/).

This database will show both vendors and buying stores, their items, place in map etc. we also introduced `@whosells` in-game command which does a simple search for items in market you can also use `@navshop` to navigate to a specific shop if you don't want to get lost!

Also auto traders will now expire after 7 days this to clean up space and get rid of dead ones.

## Auto commands
We have introduced auto commands you can now set up to 10 commands to be executed whenever you log in game you can set those by whispering to `NPC:AutoCommands`

## Show rare
We have introduced a new command `@showrare` which can be set to show a special announcement and effect on character if an item with specific rate or lower and/or specific count of options dropped or autolooted.

## Play time
We have introduced a new `@playtime` command which will show you for how long you've been online in-game! This can be seen in control panel as well.

## Refine UI
We have introduced the new refine user interface this can be accessed by talking to any of the current refine NPCs this will allow you to refine items easily without all the awkward NPC dialogs and doesn't require you to be able to wear this specific item.

# Content
## EXP Table

we've changed to a new exp table, this new exp table follows the last known exp table before trans update its not custom one and it follows a different experience curve, the older exp table had a 25% growth level on level starting from level 1 while the new one has 7 ~ 7.5% growth from 1 to 79 and 17 ~ 25% onward with a total of roughly 430 million in total this means leveling now up to 80 is a lot easier than previously but from this point on it will be harder this new curve will allow new players to easily catch up while giving them a good challenge reaching highest levels.

For this we also adjusted all players levels most players has gained new levels while higher level players might have lost few levels, the following table show the exact increase or decrease in levels assuming you're at 0% EXP if you have already some experience towards next level you may gain more levels.

Considering the huge change in experience requirements we have also lowered experience penalty to 3% from 10%.

| Old level | New Level |
| :-------- | :-------- |
| 0         | 1         |
| 1         | 1         |
| 2         | 1         |
| 3         | 2         |
| 4         | 3         |
| 5         | 4         |
| 6         | 5         |
| 7         | 6         |
| 8         | 7         |
| 9         | 8         |
| 10        | 9         |
| 11        | 10        |
| 12        | 11        |
| 13        | 12        |
| 14        | 13        |
| 15        | 14        |
| 16        | 15        |
| 17        | 16        |
| 18        | 18        |
| 19        | 19        |
| 20        | 20        |
| 21        | 21        |
| 22        | 22        |
| 23        | 23        |
| 24        | 24        |
| 25        | 25        |
| 26        | 26        |
| 27        | 27        |
| 28        | 29        |
| 29        | 30        |
| 30        | 31        |
| 31        | 32        |
| 32        | 33        |
| 33        | 34        |
| 34        | 35        |
| 35        | 36        |
| 36        | 38        |
| 37        | 39        |
| 38        | 40        |
| 39        | 41        |
| 40        | 42        |
| 41        | 43        |
| 42        | 44        |
| 43        | 46        |
| 44        | 47        |
| 45        | 48        |
| 46        | 49        |
| 47        | 50        |
| 48        | 51        |
| 49        | 52        |
| 50        | 54        |
| 51        | 55        |
| 52        | 56        |
| 53        | 57        |
| 54        | 58        |
| 55        | 59        |
| 56        | 60        |
| 57        | 62        |
| 58        | 63        |
| 59        | 64        |
| 60        | 65        |
| 61        | 66        |
| 62        | 67        |
| 63        | 68        |
| 64        | 70        |
| 65        | 71        |
| 66        | 72        |
| 67        | 73        |
| 68        | 74        |
| 69        | 75        |
| 70        | 76        |
| 71        | 78        |
| 72        | 79        |
| 73        | 80        |
| 74        | 81        |
| 75        | 81        |
| 76        | 82        |
| 77        | 83        |
| 78        | 83        |
| 79        | 84        |
| 80        | 84        |
| 81        | 85        |
| 82        | 86        |
| 83        | 86        |
| 84        | 87        |
| 85        | 87        |
| 86        | 87        |
| 87        | 88        |
| 88        | 88        |
| 89        | 89        |
| 90        | 89        |
| 91        | 90        |
| 92        | 90        |
| 93        | 91        |
| 94        | 91        |
| 95        | 91        |
| 96        | 92        |
| 97        | 92        |
| 98        | 93        |
| 99        | 93        |

## Poring Village
Poring village is now in you can access this new instance to farm jello fragments which can later be turned into jello stones thus will later be used to create and upgrade various gears so start hunting now.

This instance is accessible for players from level 30 to 60 and cooldown resets every day at 4am server time every player can get up to 2 jello fragments in single run and for each unique player in the instance the rewards doubles for example if the instance is done with a full unique party you can get between 12 ~ 24 fragments per run, every day drops specific type of jello which can later be combined into jello stones.

Every 5 jellos can be combined into 1 jello stone it takes 24 hours to finish combining them and you can speed up this process instantly by either spending 250k zeny or 1 Gelstar.

The poring village entrance can be found at `prt_fild05,145,235` for first timers you will receive a special low headgear which can be later enchanted using the NPC found at `prt_fild05,174,238`.

The jello fragments combiner can be found in the memorial workshop accessible through Prontera at `prontera,50,227`

## Thor's Flame
Thor's flame is a new exchange NPC allows you to exchange normal refine ores into enriched or HD ones also exchanging some trash into more valuable materials.

### Refine ores
| Material            | Exchange fee                  | Out material        |
| :------------------ | :---------------------------- | :------------------ |
| 1 Oridecon          | 500,000 Zeny or 1 Gel star    | 1 Enriched Oridecon |
| 1 Elunium           | 500,000 Zeny or 1 Gel star    | 1 Enriched Elunium  |
| 1 Enriched Oridecon | 1,000,000 Zeny or 4 Gel stars | 1 HD Oridecon       |
| 1 Enriched Elunium  | 1,000,000 Zeny or 4 Gel stars | 1 HD Elunium        |
| 1 Bradium           | 1,500,000 Zeny or 5 Gel stars | 1 HD Bradium        |
| 1 Cranium           | 1,500,000 Zeny or 5 Gel stars | 1 HD Cranium        |

### Minerals exchange
#### Lesser Minerals
For 5 Iron or 5 Iron Ore you can exchange them randomly to one of those items

* 2x Phracon
* 1x Emveretarcon
* 1x Steel
* 1x Elunium Ore
* 1x Oridecon Ore

#### Intermediate Mineral (Disabled currently)
For 5 Steel or 5 Coal you can exchange them randomly to one of those items

* 2x Elunium Ore
* 2x Oridecon Ore
* 3x Elunium
* 3x Oridecon
* 1x Gold

#### Greater Mineral (Elemental Stones)
For 3 Elunium or 3 Oridecon you can exchange them randomly to one of those items

* 3x Red blood
* 3x Crystal Blue
* 3x Wind of Verdure
* 3x Yellow Live
* 1x Flame Heart
* 1x Mystic Frozen
* 1x Rough Wind

#### Basic jewels
For 7 Elunium or 7 Oridecon you can get a random basic jewel:

* 1 ~ 2x Garnet
* 1 ~ 2x Amethyst
* 1 ~ 2x Emerald
* 1 ~ 2x Pearl
* 1 ~ 2x Ruby
* 1 ~ 2x Sardonyx
* 1 ~ 2x Opal
* 1 ~ 2x Topaz
* 1 ~ 2x Zircon
* 1 ~ 2x 2 Carat Diamond
* 1 ~ 2x Aquamarine

#### Special jewel exchange
You can exchange 2 of one special jewel for one random special jewel. Also has a chance to change into Gold or an Emperium Anvil. The qualifying special jewels are as follows:

* Olivine
* Phlogopite
* Agate
* Muscovite
* Rose Quartz
* Turquoise
* Citrine
* Pyroxene

## Skill Balances
### Bard/Dancer
#### Musical Strike / Throw Arrow
Now it deals 2 hits and attack ratio changed to 10 + 40 * skill level% ATK, Fixed Cast time is removed and Cast time lowered to 0.5 seconds and 0.3 seconds global cooldown added.

### Alchemist
##### Cannibalize
The attack of summoned monsters has been highly improved and Parasite can now do double attacks

# Changes
* Search results in-game from @ii/@mi and similar has been reduced back to 5 results, please use the website database.
* Autotrade is now only available inside cities
* If a player tries to kill steal a monster a new visual effect of a shield will show up on the monsters for the KSer only 

# Fixes
* Kill steal protection should now work as expected
* Sea Witch Foot, Battle Hook and Koronos is no longer wearable by non-trans classes and no longer dropped, the items will be kept with the users to be used when trans later release.
* Fixed Infinity Drink effect now it correctly increase magical and physical attacks, enables no cast cancelation and increases maxp HP and SP as expected
* Hunter traps weight corrected to 0.2 and Blue/Yellow/Red Gemstones to 0.1
* Corrected Orcish and Oak mace dropped options to be in magic pool, previously dropped items won't be rerolled.
* Backstap will no longer attack if it's not possible to slide behind enemies
* Backstap will now slide in PvP environment maps
* Desert Frilldolars element has been fixed to earth 2
* Increased attack on normal and boss monsters options should now correctly increase the attack damage rate
* Guild expansion now correctly adds 4 slots per level guilds with members more than 56 have all of their members kicked and they will have to re-invite
* Trunk drops has been added back to Elder Willow
* Fury status has been corrected to only decrease SP regeneration by 50% instead of disabling it
* Asura strike shall no longer stop SP regeneration for 5 minutes
* Bowling bash shall no longer attack unreachable monsters
* Dragonology will now give +1 int per skill level learned
* Neutral element resistance has been removed from fever shoes and re-rolled into formless tolerance
* Hylozoist Card has been temporary disabled until fixed
* Harcy NPC would no longer allow to keep repeating of the banana quest
* Fixed Increase agility not giving ASPD modifier as expected
* Fixed teleport skill teleporting into NPCs (specifically warps)

