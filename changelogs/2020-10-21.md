# Changelogs for Patch 1.1.3
This patch delivers new permanent content with the CuPet Update, as well as a new event: Halloween!
Additionally, you will find a ton of bugfixes and improvements in this patch. Let's dive in.

# Event
## Halloween
The Halloween season is upon us for the first time in the world of Project Zero!
Discover the tragic lives of Sohees and why they're wandering around so sad.

Finishing the mission for the first time on a character allows them to do the daily Halloween memorial dungeon.
This dungeon can be only done once per Master Account per day.
Finishing the dungeon gives you a chance to get one of the new exclusive Halloween costumes or Halloween Coins.

The Halloween MD Treasure Box can drop the following:
* Common
    * 1 ~ 2x Halloween Coin
    * 1x Costume: Gothic Pumpkin Head
* Uncommon
    * 1x Costume: Diabolic Lapel
    * 1x Costume: Stall of bat
    * 1x Costume: Fallen Angel Valletta
* Rare
    * 1x Costume: Secret Zipper
    * 1x Costume: Halloween Hat
* Epic
    * 1x Costume: Halloween Poring Bag

You will also always get one guaranteed Halloween Coin.
What will those coins be used for? Not sucking your souls.

We figured out that the last event simply sucked the souls of many people, grinding it non-stop, leaving no energy for Halloween!
So hopefully this new coin system will work better and guarantee people at last some costumes out of this event.
As scary as the dungeon instance is, the boss fight is not too difficult. But the HP pool is quite large, so make sure to bring DPS! 

The event can started at Ill Sohee (`/navi pay_dun03 144/143`), and your Halloween Coins can be exchanged at Sakray (`/navi niflheim 201/205`).

The event is expected to be available until late Novmber!

# Content
## Cash Shop
We have finally launched our Cash Shop!
If you would like to add some Cash Points to your account, head over to your [Control Panel](https://cp.playragnarokzero.com/) and select the ["Top up!"](https://cp.playragnarokzero.com/topup/) action from the left-hand menu.

You can spend those Cash Points on the Cash Shop ingame. So go and get your costume on!

We're also working on bringing Paypal and Credit Card options as soon as possible.
Until then, please refer to the alternative payment methods available. Mint Prepaid Cards are sold by many official resellers listed on the Mint website.
Also, our players in Europe and SEA should have a lot of local options including bank transfers (like iDEAL in the Netherlands, SOFORT in Germany, Belgium, France, ...) available!

The following is a preview of the current available items:
* ![Gossip Raven](https://i.gyazo.com/9ef8437f82b33ce5239f33d4ca7ed26e.gif)
* ![Ordinary Black Magician Hat](https://i.gyazo.com/3e285dd343caa86c42402d5e83fc385e.gif)
* ![Gerhard Von Devi](https://i.gyazo.com/c422743f9fa9c3ee1ab671b76a2ade65.gif)
* ![There is something](https://i.gyazo.com/6ec95a047837e025f838e7ba204200f9.gif)
* ![Niflheim Bunny Hat](https://i.gyazo.com/18494705ddbe72c2d2e23d2b12e03f8a.gif)
* ![Ghostring Tall Hat](https://i.gyazo.com/5f05bcc5b458714375450311a328a937.gif)
* ![Gram Peony](https://i.gyazo.com/2ad89e96172d7c41d16c74bf61ed571b.gif)
* ![Vampire Familiar](https://i.gyazo.com/bdc33d63cedeb01935cb64fd31690992.gif)

## CuPet update
The ability to tame, hatch and evolve CuPets (or just "Pets") has been ingame for a while now. However, we have now updated our Pets to be on par with the first major kRO Zero pet overhaul. Changes include new pets, evolutions and adjusted requirements.

### New pets and taming items
* Mummy can be tamed with [Spiritual Bandage](https://cp.playragnarokzero.com/item/view/?id=19376), dropped from Evil Druid
* Loli Ruri can be tamed with [Very Red Juice](https://cp.playragnarokzero.com/item/view/?id=12360), dropped from Sky Petite
* Whisper can be tamed with [Ghost Coffin](https://cp.playragnarokzero.com/item/view/?id=12363), dropped from Whisper
* Wanderer can be tamed with [Vagabond Skull](https://cp.playragnarokzero.com/item/view/?id=14574), dropped from Alarm
* Stone Shooter can be tamed with [Oilpalm Coconut](https://cp.playragnarokzero.com/item/view/?id=12369), dropped from Stone Shooter
* Bongun can be tamed with [Her Heart](https://cp.playragnarokzero.com/item/view/?id=659), which can be traded with Candina the Pet Expert in Comodo (`/navi comodo 112/182`)

### New pet food
All Pet Groomer NPCs in various cities now have the same inventory. They now sell all types of new Pet Food that your pet could possibly ask for!

### New pets and evolutions
We have updated all pet evolution materials to match kRO Zero, which includes the need for Mythril in all of them.
Please check out our Wiki for details on all the new pets, effects and evolution requirements: https://wiki.playragnarokzero.com/wiki/Cute_Pet_System

## Guild Dungeons
The first two Guild Dungeons have arrived! Form a party with your guildmates and explore the Guild Dungeons of the Valkyrie (Prontera) and Britoniah (Geffen) Realms!

Guild Dungeons can still be accessed for free by any Guild that owns in a Castle in that Realm. However, we have not had a War of Emperium yet.
But don't fret! Guild Dungeons will also be accessible for *anyone* through a special NPC in each Realm. All you have to do is ~~bribe~~ pay them 30,000 Zeny.
The NPCs can be found here:
* Valkyrie Realm (Prontera): `/navi prt_gld 156/99`
* Britoniah Realm (Geffen): `/navi gef_fild13 195/241`

If you have been in Guild Dungeons before, you will notice that the monsters have significantly increased in terms of strength, so be prepared!
The challenge will be worth it, though. Apart from good EXP, the monsters in Guild Dungeon will also drop invaluable supplies for the upcoming War of Emperium.
A new currency, "Guild Dungeon Coins", can be obtained from the new Guild Dungeons and then traded in the new Guild Office (see below).

One last but very important note about the new Guild Dungeons:
The maps have a special version of GvG in place, meaning that players from opposing guilds can kill each other - even when in the same party!
So try not to make (more) enemies.

### Guild Office
As we are nearing our first War of Emperium, the new Guild Office has already set up in south-west Prontera (`/navi prt_in 210/161`).

The Guild Dungeon Coins that you collect in Guild Dungeons can be traded here for WoE-specific weapons, armor and potions.
Make sure to check out what they have in store and prepare your guild for the upcoming War of Emperium!

In a later content update, the Guild Office will also be expanded to craft upgraded versions of their gear, as well as new ways of enchanting it.

## New items
* Implemented the First Aid Boxes
    * Every 5 levels or so, you can open a new First Aid Box to receive some items that can will help your leveling journey.
    * New characters will automatically get the first box in their inventory
    * Existing characters can receive the box by speaking to the First Aid Helper in Izlude (`/navi izlude 121/153`)
* Alice now drops [Wild Rose Hat](https://cp.playragnarokzero.com/item/view/?id=5557)
* Quve now drops [Quve Mask](https://cp.playragnarokzero.com/item/view/?id=5475)
* Mimic now drops four new elemental books:
    * [Genealogy of Thunder](https://cp.playragnarokzero.com/item/view/?id=1592)
    * [Genealogy of Flame](https://cp.playragnarokzero.com/item/view/?id=1594)
    * [Genealogy of Ice](https://cp.playragnarokzero.com/item/view/?id=1595)
    * [Earth Pedigree Book](https://cp.playragnarokzero.com/item/view/?id=1596)

# Quality of Life
* Lowered Restat System cooldown from 90 to 30 days
* Introduced tiered-zeny Restat System, starting at 5m, doubling with each use and capping at 20m per restat
* Adjusted drop range for Azure/Crimson Crystals from 0-8 to 1-8 for Ant Hell [E], Izlude [E] & Sunken Ship [E]
* Adjusted kill count map announcements to be less exploitable
* Attendance Screen will only open when there's claimable rewards
* Added `@daily` command which will show your cooldowns for daily instances and time until reset

# Changes
* Aligned drop rate of some new Fever gear, namely Prison Watcher, Royal Knight's Broadsword, Royal Knight's Lance
* Alice now drops Katar with options
* Majoruros now drops Fist with options
* Assaulter now drops Jur with options
* Dark Orc Hero now drops Light Epsilon with options
* Dark Orc Lord now drops Bloody Axe with options
* Changed buy price of Cheese to 28 Zeny
* Changed buy price of Level 1 ~ 5 Cookbooks to 750 Zeny (from 1000 Zeny)
* Adjusted the despawn time of Awakened Ghosts in Glastheim Staircase to match champion mobs
* Disabled dueling for players that are vending, buying or have a chat room open
* Adjusted the Novice skill "Play Dead" according to kRO Zero:
    * The duration is now a fixed 30 seconds
    * It has a cooldown of 60 seconds
    * Play Dead can no longer be stopped manually before the duration is up
* `@duel` Would no longer work on vendors, buying stores and players in chat rooms.
* Corrected the noks protection on Champions once more and they should be back to their normal rates

# Fixes
* Fixed [Event] potions so they can be put into master storage
* Added missing slots to some new equipment: Dullahan Glove, Rabbit Shirt, Safety Boots, Royal Knight's Broadsword, Royal Knight's Lance
* Fixed issue where Jewel Shield could be equipped before level 60
* Fixed Maya Card description inaccuracy & Prefix
* Fixed Maya Card effect from Increase damage taken from fire/dark by -15% to -20%
* Fixed Poring Village Carrot/Leek to trigger effect when using weapon skills
* Fixed cards that specifically drain SP with melee attacks to now correctly drain on melee attacks only

# Skill fixes
* Fixed cast time of "Finger Offensive" to be 0.5s Variable and 0.5s Fixed Cast Time
* Fixed PvP-only Bard and Dancer skills to only work on PvP maps
* Fixed Dancer's "Dont Forget Me" to properly decrease enemy ASPD. Corrected the range to 9x9 as well
* Fixed SP cost of "Musical Strike" and "Throw Arrow" to 15 SP for all levels
* Fixed "Adaptation to Circumstances" to not have a cast time and an SP cost of 10
* Fixed variable cast time of "Into the Abyss" and "Invulnerable Siegfried" to 1s
* Fixed passive skill bonuses for "Musical Lesson", "Dancing Lessons" and "Plagiarism" sometimes not being calculated correctly
* Fixed Bard and Dancer not having their last performance saved (for Encore) when they're not in a party
* Summoned Alchemist planned can no longer be targeted
* Fixed "Venom Splasher" not carrying the weapon element
