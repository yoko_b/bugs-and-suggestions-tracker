# Changelogs for Patch 1.1.7

This patch delivers new cash shop costumes, Stylist and Homunculus. Let's dive in.

# Content

## Homunculus System
The Homunculus is a special pet which assists Alchemist classes in various ways. Alchemist classes must go through the Bioethics Quest to unlock the appropriate skills required to create a Homunculus. After the quest, the passive skill Bioethics will be available to the player and the rest of the Homunculus skill branch will be available through the distribution of skill points.

* Based on our kRO Zero research, we determined that the Bioethics quest moved to the Alchemist Guild in Al De Baran. We've moved it as well, which means we now have Homunculus! See the [Bioethics Quest](https://wiki.playragnarokzero.com/wiki/Alchemist_Skill_Quest#Bioethics_Quest) page on the wiki for the walkthrough. Also check out the [Homunculus System](https://wiki.playragnarokzero.com/wiki/Homunculus_System) page for more information about Homunculus.

* The Homunculus gets 10% of any experience you get. This doesn't mean they will actually cut of your experience but it means that you will get 100% of the EXP and 10% of what you're getting also goes to the Homunculus. You can level up Homunculus in parties (even share), and through quests as well.

## Elemental Resist Potion Creation
Alchemists can also now complete the quest to get the manual for making Elemental Resist Potions. See [this wiki page for the walkthrough](https://wiki.playragnarokzero.com/wiki/Alchemist_Skill_Quest#Resistance_Potions_Guide_Quest).

## Stylist
At last the Stylist has arrived with 209 cloth color and 127 hair color. There has never been a better time to unleash the artist within!

You can find the Stylist at `/navi prt_in 244/164`. Also any cloth color you buy will be kept in a wardrobe, if you ever wish to change to it for free!

## Halloween event
The event has ended, the exchange NPC for Halloween will be kept until next maintenance.

# Cash shop
We've added the first batch of Christmas Winter collection, this collection should be available until sometime mid month

* ![Louis Red Hat](https://i.gyazo.com/0ca33b9f4a628ed73673344171cec44a.gif)
* ![Floating Ice](https://i.gyazo.com/eb3315aab28a7f212026ed22de8b7766.gif)
* ![Fluffy Angel cap](https://i.gyazo.com/2b1d145a158a36763434825d68112823.gif)
* ![Red Cat Ears cap](https://i.gyazo.com/6479c11d8019b309d2bfc28f21be5710.gif)
* ![Rabbit Knit Hat](https://i.gyazo.com/3d25e10661ed959ee0f3d4e77fe80809.gif)
* ![Fluffy semi-long hat](https://i.gyazo.com/11d34fbd79153acfcd07c08df51bde43.gif)
* ![Santa Backpack](https://i.gyazo.com/668d147b3dbc347ea3269d8925e489dd.gif)

# Fixes
* HD ores should be tradeable now.
* PvP Items from guild dungeons should now work in the GvG Arena.

# WoE
We have disabled Treasure Boxes until further notice while we work on improving them.

* Alliances no longer work inside Guild Castles, however it will still affect guild dungeons.
* The no dual rule is updated to cover all castles instead of one castle.
* Costumes have been disabled inside WoE Castles.
