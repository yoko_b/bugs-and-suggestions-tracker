# Changelogs for Patch 1.3.1

This maintenance was mainly focused on delivering fixes related to WoE.

# Fixes
* Corrected a logic error in Ninja skill `First Wind` caused the formula to have an additional `Skill level * 100%` attack ratio
* Stylist will no longer give you a free new look until a relog in some cases
* Instance re-join behavior on disconnect has been made harsher to reduce the level of possible abuse ***NOTE: we're aware of some issues with it under specific conditions, please contact a GM to help you if you ever encountered***.
* Fixed Alliances still working in Guild castlesl.
* Fixed vit affecting Curse immunity in renewal in a very significant way.
* Fixed castles economy and defense levels starting at 0, they shall now start at 1.
* Corrected Yin yang katar being worn on a single hand instead of two.
* Fixed Crow Tengu and Fish Monster's Grill being refinable (refines have been removed as well).

# Important notice
It has came to our attention that Sropho card in-fact does not exists in kRO: Zero while other cards from Izlude dungeon drops as expected in kRO:Zero, for this matter we will disable Sropho card in-game at the moment of publishing this change logs, a further announcement shall come soon to give the final verdict on how this card shall be removed and if player will be compensated.
